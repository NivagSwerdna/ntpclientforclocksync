#include <Timezone.h>
#include <TimeLib.h>
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include <Wire.h>
#include "secrets.h"

//#define DEBUG 1
#define MESSAGES_TO_OUTPUT (60*5)
#define BAUD_RATE 4800

//#define EST_TZ
#define UK_TZ 1

char ssid[] = WIFI_SSID;  //  your network SSID (name)
char pass[] = WIFI_PASSWORD;       // your network password

const byte buff_len = 90;
char CRCbuffer[buff_len];

unsigned int localPort = 2390;      // local port for UDP packets

IPAddress timeServerIP; // time.nist.gov NTP server

#ifdef NTP_SERVER
const char* ntpServerName = NTP_SERVER;
#else
const char* ntpServerName = "us.pool.ntp.org";
#endif

const int NTP_PACKET_SIZE = 48; // NTP time stamp is in the first 48 bytes of the message

byte packetBuffer[ NTP_PACKET_SIZE]; //buffer to hold incoming and outgoing packets

// A UDP instance to let us send and receive packets over UDP
WiFiUDP udp;

unsigned long secsSince1900;

void setup()
{
  Serial.begin(BAUD_RATE);
  Wire.pins(0, 2);
  Wire.begin();
  delay(10);
  Serial.println();
  Serial.println();

  // We start by connecting to a WiFi network
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, pass);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");

  Serial.println("WiFi connected");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  Serial.println("Starting UDP");
  udp.begin(localPort);
  Serial.print("Local port: ");
  Serial.println(udp.localPort());
  setNTPtime();
  // Remove comments on next line for production
  ESP.deepSleep(3600000000, RF_DEFAULT);
}
void loop()
{
  //Comment out next two lines for production
  //delay(60000);
  //setNTPtime();
}

static uint32 realtime_stamp = 0;
LOCAL os_timer_t sntp_timer;

void ICACHE_RAM_ATTR sntp_time_inc (void)
{
  realtime_stamp++;
}

void setNTPtime()
{
  TimeChangeRule *tcr;


#ifdef EST_TZ
  // US Eastern Time Zone (New York, Detroit)
  TimeChangeRule usEDT = {"EDT", Second, Sun, Mar, 2, -240};  // Eastern Daylight Time = UTC - 4 hours
  TimeChangeRule usEST = {"EST", First, Sun, Nov, 2, -300};   // Eastern Standard Time = UTC - 5 hours
  Timezone timezone(usEDT, usEST);
#endif

#ifdef UK_TZ
  // United Kingdom (London, Belfast)
  TimeChangeRule BST = {"BST", Last, Sun, Mar, 1, 60};        // British Summer Time
  TimeChangeRule GMT = {"GMT", Last, Sun, Oct, 2, 0};         // Standard Time
  Timezone timezone(BST, GMT);
#endif

  time_t epoch = 0UL;

  boolean gotFix = false;
  while (!gotFix) {
    epoch =  getFromNTP();
    if (epoch != 0) {
      gotFix=true;
    } else
    {
      // Back off 5 seconds until next attempt
      delay(5000);
    }
  }

  // Set repeat timer.. we have just missed the second epoch so we will not worry until we see the next one in approx 1s from now
  os_timer_disarm(&sntp_timer);
  os_timer_setfn(&sntp_timer, (os_timer_func_t *)sntp_time_inc, NULL);
  os_timer_arm(&sntp_timer, 1000, 1);

  // Adjust between 1970 Unix and 1900 NTP time domains
  epoch -= 2208988800UL;

  boolean done = false;
  int messages=0;

  while (!done) {

    uint32 current_realtime_stamp = realtime_stamp;

    while (realtime_stamp == current_realtime_stamp) {
      // Lurk until next second boundary has passed (local clock)
      yield();
    }

    // It's changed so better use the new value
    current_realtime_stamp = realtime_stamp;

    time_t now = epoch + current_realtime_stamp;

    // Calculate and apply DST, and consider it might change whilst we are generating messages so check every time
    if (timezone.utcIsDST(now)) {
      now += 3600;
    }
    
    int h = hour(now);
    int m = minute(now);
    int s = second(now);
    int d = day(now);
    int mo = month(now);
    int y = year(now);

    String gpsstring = "$GPRMC," + printLeading0(h) + printLeading0(m) + printLeading0(s) + ".00,A,3500.1325,N,8200.0077,E,0.0,0.0," + printLeading0(d) + printLeading0(mo) + String(y).substring(2) + ",,*";
    outputMsg(gpsstring);

    messages++;

    if (messages>=MESSAGES_TO_OUTPUT) {
      Serial.print("That's enough for now");
      break;
    }
  }
}

unsigned long getFromNTP()
{
  WiFi.hostByName(ntpServerName, timeServerIP);
  Serial.print("NTP Server: ");
  Serial.println(timeServerIP);

  uint64_t before_send_micros64 = micros64();
  uint64_t after_send_micros64;

  sendNTPpacket(timeServerIP); // send an NTP packet to a time server

  int cb;

#ifdef DEBUG
  Serial.println("Wait for result");
#endif

  boolean timedOut = false;
  while (!timedOut) {
    cb = udp.parsePacket();
    if (!cb) {
      uint64_t now_micros64 = micros64();
      if ((now_micros64 - before_send_micros64) >= 1000000L) {
        Serial.print("Timed out");
        return 0L;
      }
      yield();
    } else
    {
      after_send_micros64 = micros64();
      break;
    }
  }

#ifdef DEBUG
  Serial.print("packet received, length=");
  Serial.println(cb);
#endif  

  uint64_t round_trip_micros64 = after_send_micros64 - before_send_micros64;
  uint64_t transit_micros64 = round_trip_micros64 / 2;

  unsigned long transitNtpFrac = (unsigned long)transit_micros64 * 4294L;

#ifdef DEBUG
  Serial.print("Roundtrip time:");
  Serial.println((int)round_trip_micros64);
  Serial.print("Transit time:");
  Serial.print((int)transit_micros64);
  Serial.print(", ");
  Serial.println((int)transitNtpFrac);
#endif  

  // We've received a packet, read the data from it
  udp.read(packetBuffer, NTP_PACKET_SIZE); // read the packet into the buffer

  //the timestamp starts at byte 40 of the received packet and is four bytes,
  // or two words, long. First, extract the two words:

  unsigned long highWord = word(packetBuffer[40], packetBuffer[41]);
  unsigned long lowWord = word(packetBuffer[42], packetBuffer[43]);
  // combine the four bytes (two words) into a long integer
  // this is NTP time (seconds since Jan 1 1900):
  unsigned long ntpSeconds = (unsigned long) highWord << 16 | lowWord;

  unsigned long highWordFrac = word(packetBuffer[44], packetBuffer[45]);
  unsigned long lowWordFrac = word(packetBuffer[46], packetBuffer[47]);
  unsigned long ntpFrac = (unsigned long) highWordFrac << 16 | lowWordFrac;

#ifdef DEBUG
  Serial.print("NTPSec:  ");
  Serial.println(ntpSeconds);
  Serial.print("NTPFrac: ");
  Serial.println(ntpFrac);
#endif  

  unsigned long adjNtpFrac = ntpFrac + transitNtpFrac;
  unsigned long adjNtpSeconds = ntpSeconds + ((adjNtpFrac < ntpFrac) ? 1 : 0);

#ifdef DEBUG
  Serial.print("AdjNTPSec:  ");
  Serial.println(adjNtpSeconds);
  Serial.print("AdjNTPFrac: ");
  Serial.println(adjNtpFrac);
#endif  

  unsigned long excessNtpFracToNextSecond = 4294967295L - adjNtpFrac;
  unsigned long excessMicrosToNextSecond = excessNtpFracToNextSecond / 4294L;
  unsigned long excessMillisToNextSecond = excessMicrosToNextSecond / 1000L;
  excessMicrosToNextSecond -= excessMillisToNextSecond * 1000L;

#ifdef DEBUG
  Serial.print("excessMillisToNextSecond:  ");
  Serial.println((int)excessMillisToNextSecond);
  Serial.print("excessMicrosToNextSecond: ");
  Serial.println((int)excessMicrosToNextSecond);
#endif  

  if ((excessMillisToNextSecond > 0) || (excessMicrosToNextSecond > 0)) {
#ifdef DEBUG
    Serial.println("Start Faffing...");
#endif  

    // Fritter away some time until the next second
    if (excessMillisToNextSecond > 0) {
      // This will yield safely
      delay(excessMillisToNextSecond);
    }
    if (excessMicrosToNextSecond > 0) {
      // Less than 1ms so we can safely starve other threads
      delayMicroseconds(excessMicrosToNextSecond);
    }
#ifdef DEBUG
    Serial.println("... done Faffing");
#endif  
    adjNtpSeconds++;
  }

#ifdef DEBUG
  Serial.print("FinalAdjNTPSec:  ");
  Serial.println(adjNtpSeconds);
#endif  

  return adjNtpSeconds;
}

// send an NTP request to the time server at the given address
unsigned long sendNTPpacket(IPAddress& address)
{
#ifdef DEBUG
  Serial.println("sending NTP packet...");
#endif  
  // set all bytes in the buffer to 0
  memset(packetBuffer, 0, NTP_PACKET_SIZE);
  // Initialize values needed to form NTP request
  // (see URL above for details on the packets)
  packetBuffer[0] = 0b11100011;   // LI, Version, Mode
  packetBuffer[1] = 0;     // Stratum, or type of clock
  packetBuffer[2] = 6;     // Polling Interval
  packetBuffer[3] = 0xEC;  // Peer Clock Precision
  // 8 bytes of zero for Root Delay & Root Dispersion
  packetBuffer[12]  = 49;
  packetBuffer[13]  = 0x4E;
  packetBuffer[14]  = 49;
  packetBuffer[15]  = 52;

  // all NTP fields have been given values, now
  // you can send a packet requesting a timestamp:
  udp.beginPacket(address, 123); //NTP requests are to port 123
  udp.write(packetBuffer, NTP_PACKET_SIZE);
  udp.endPacket();
}

String printLeading0(int n)
{
  if (n < 10) return "0" + String(n);
  else
    return String(n);
}

void outputMsg(String msg) {
  msg.toCharArray(CRCbuffer, sizeof(CRCbuffer)); // put complete string into CRCbuffer
  int crc = convertToCRC(CRCbuffer);
  Serial.print(msg);
  if (crc < 16) Serial.print("0"); // add leading 0
  Serial.println(crc, HEX);
}

byte convertToCRC(char *buff) {
  // NMEA CRC: XOR each byte with previous for all chars between '$' and '*'
  char c;
  byte i;
  byte start_with = 0;
  byte end_with = 0;
  byte CRC = 0;

  for (i = 0; i < buff_len; i++) {
    c = buff[i];
    if (c == '$') {
      start_with = i;
    }
    if (c == '*') {
      end_with = i;
    }
  }
  if (end_with > start_with) {
    for (i = start_with + 1; i < end_with; i++) { // XOR every character between '$' and '*'
      CRC = CRC ^ buff[i] ;  // compute CRC
    }
  }
  return CRC;
}
