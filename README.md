# NTPClientForClockSync

An accurate (within a few tens of ms on a good day) SNTP client for syncing local clocks (nixies etc) to NTP time servers

For best performance create a NTP server on your local network (A RPi would be nicely) and use this for synchronisation

The code works on the smallest/cheapest ESP-01 and should also work on more capable ESP8266 hardware and above.

Acknowledgement: 
Based on some original work published at: http://www.eevblog.com/forum/microcontrollers/any-full-ntp-client-for-esp8266esp32/msg1804631/#msg1804631)

Share & Enjoy
Nivag